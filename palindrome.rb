#check if word is palindrome or not

def palindrome?(word)
  return (word == word.reverse) ? true : false
end

puts "Enter a word to see if it's a palindrome..."
word = gets.chomp

puts "The word #{word} is #{palindrome?(word.to_s) ? 'a' : 'not a'} palindrome."