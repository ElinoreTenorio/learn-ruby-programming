#list pictures in a specific directory

path = 'C:/albums'
valid_files = ['.png', '.jpg', '.jpeg']
Dir.foreach(path) { |folder| 
  next if ['.', '..'].include? folder
  puts "#{folder}" 
  Dir.foreach("#{path}/#{folder}") { |subfolder|
    puts "- #{subfolder}" if valid_files.include? File.extname(subfolder)
  }
}